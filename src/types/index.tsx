export interface ItemProps {
  date: string;
  hits: number;
  unique: number;
  registrations: number;
  demo_registrations: number;
  conversion: number;
  deposit: number;
  ftd: number;
  deals: number;
  profit: number;
  [key: string]: any;
}

export interface DateParams {
  from: string;
  to: string;
  min: string;
  max: string;
}
