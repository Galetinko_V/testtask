import React from 'react';

import { ItemProps } from '../../../types';

import './style.css';

interface TableFooterProps {
  data: ItemProps[];
  hiddenData: string[];
  itemKeys: string[];
}

export class TableFooter extends React.PureComponent<TableFooterProps> {
  render() {
    const { data, hiddenData, itemKeys } = this.props;

    return (
      <tfoot>
        <tr className='table-footer'>
          {!!itemKeys.length && <td>Total</td>}
          {itemKeys
            .slice(1)
            .filter((key) => !hiddenData.includes(key))
            .map((key) => (
              <td key={`total_${key}`}>
                {data.reduce((acc, item) => acc + item[key], 0)}
              </td>
            ))}
        </tr>
      </tfoot>
    );
  }
}
