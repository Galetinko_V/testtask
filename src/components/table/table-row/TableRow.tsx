import React from 'react';

import { ItemProps } from '../../../types';
import { getColName } from '../../../utils';

import './style.css';

interface TableRowProps {
  item: ItemProps;
  itemKeys: string[];
  hiddenData: string[];
}

interface TableRowState {
  showHidden: boolean;
}

export class TableRow extends React.PureComponent<
  TableRowProps,
  TableRowState
> {
  state = { showHidden: false };

  componentDidUpdate(prevProps: TableRowProps) {
    const { hiddenData } = prevProps;

    if (!hiddenData.length || prevProps.item !== this.props.item) {
      this.setState({ showHidden: false });
    }
  }

  onChangeHidden = (): void => {
    this.setState((prevState) => ({ showHidden: !prevState.showHidden }));
  };

  render() {
    const { item, itemKeys, hiddenData } = this.props;
    const { showHidden } = this.state;
    const hasButton = hiddenData.length !== 0;

    return (
      <>
        <tr className='table-row'>
          {itemKeys
            .filter((key) => !hiddenData.includes(key))
            .map((key, index) =>
              key !== 'date' ? (
                <td key={`table-column_${key}`}>{item[key]}</td>
              ) : (
                <td key={`table-column_${key}`} className='position-relative'>
                  <span>{item[key]}</span>
                  {hasButton && (
                    <div className='btn-container'>
                      <button
                        className='btn-details'
                        onClick={this.onChangeHidden}
                      >
                        More details
                      </button>
                    </div>
                  )}
                </td>
              )
            )}
        </tr>
        {showHidden &&
          hiddenData.map((key, index) => (
            <tr key={`hidden-table-column${index}`}>
              <td>{getColName(key)}</td>
              <td
                className='pl-5'
                align='left'
                colSpan={itemKeys.length - hiddenData.length - 1}
              >
                {item[key]}
              </td>
            </tr>
          ))}
      </>
    );
  }
}
