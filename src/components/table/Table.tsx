import React from 'react';

import TableHeader from './table-header';
import TableBody from './table-body';
import TableFooter from './table-footer';

import { ItemProps } from '../../types';

import './style.css';

interface TableProps {
  data: ItemProps[];
  itemKeys: string[];
}

interface TableState {
  windowWidth: number;
  hiddenData: string[];
  sortParams: { key: string; direction: string };
}

export class Table extends React.Component<TableProps, TableState> {
  state = {
    windowWidth: window.innerWidth,
    hiddenData: [],
    sortParams: {
      key: '',
      direction: '',
    },
  };

  componentDidMount() {
    window.addEventListener('resize', this.onChangeWindowWidth);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onChangeWindowWidth);
  }

  componentDidUpdate() {
    const { windowWidth, hiddenData } = this.state;
    const { itemKeys } = this.props;
    const newHiddenData = this.getHiddenData(windowWidth, itemKeys);

    if (hiddenData.length !== newHiddenData.length) {
      this.setState({ hiddenData: newHiddenData });
    }
  }

  getHiddenData(windowWidth: number, itemKeys: string[]) {
    switch (true) {
      case windowWidth <= 450:
        return itemKeys.slice(-8).slice(0, -1);
      case windowWidth <= 550:
        return itemKeys.slice(-7).slice(0, -1);
      case windowWidth <= 700:
        return itemKeys.slice(-6).slice(0, -1);
      case windowWidth <= 850:
        return itemKeys.slice(-5).slice(0, -1);
      case windowWidth <= 950:
        return itemKeys.slice(-4).slice(0, -1);
      case windowWidth <= 1000:
        return itemKeys.slice(-3).slice(0, -1);
      case windowWidth <= 1100:
        return itemKeys.slice(-2).slice(0, -1);
      default:
        return [];
    }
  }

  onChangeWindowWidth = (event: UIEvent): void => {
    const target = event.target as Window;
    this.setState({ windowWidth: target.innerWidth });
  };

  onChangeSort = (key: string, direction: string): void =>
    this.setState({ sortParams: { key, direction } });

  render() {
    const { data, itemKeys } = this.props;
    const { hiddenData, sortParams } = this.state;

    return (
      <table className='table table-striped'>
        <TableHeader
          hiddenData={hiddenData}
          itemKeys={itemKeys}
          sortParams={sortParams}
          onChangeSort={this.onChangeSort}
        />
        <TableBody
          data={data}
          hiddenData={hiddenData}
          itemKeys={itemKeys}
          sortParams={sortParams}
        />
        <TableFooter data={data} hiddenData={hiddenData} itemKeys={itemKeys} />
      </table>
    );
  }
}
