import React from 'react';

import TableRow from '../table-row';

import { ItemProps } from '../../../types';

interface TableBodyProps {
  data: ItemProps[];
  hiddenData: string[];
  itemKeys: string[];
  sortParams: { key: string; direction: string };
}

export class TableBody extends React.PureComponent<TableBodyProps> {
  sortData = (
    key: string,
    direction: string,
    data: ItemProps[]
  ): ItemProps[] => {
    if (key === '') {
      return data;
    }

    const byAscending = direction === 'down' ? 1 : -1;

    return key !== 'date'
      ? data.sort((a, b) => byAscending * (b[key] - a[key]))
      : data.sort(
          (a, b) => byAscending * (+new Date(b[key]) - +new Date(a[key]))
        );
  };

  render() {
    const { sortParams, data, hiddenData, itemKeys } = this.props;
    const { key, direction } = sortParams;

    return (
      <tbody>
        {this.sortData(key, direction, data).map((item, index) => (
          <TableRow
            key={`table-row${index}`}
            item={item}
            hiddenData={hiddenData}
            itemKeys={itemKeys}
          />
        ))}
      </tbody>
    );
  }
}
