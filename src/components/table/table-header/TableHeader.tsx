import React from 'react';

import SortButton from '../../sort-button';

import { getColName } from '../../../utils';

interface TableHeaderProps {
  itemKeys: string[];
  hiddenData: string[];
  sortParams: { key: string; direction: string };
  onChangeSort: (key: string, direction: string) => void;
}

export class TableHeader extends React.PureComponent<TableHeaderProps> {
  render() {
    const { itemKeys, hiddenData, sortParams, onChangeSort } = this.props;

    return (
      <thead>
        <tr>
          {itemKeys
            .filter((item) => !hiddenData.includes(item))
            .map((item) => (
              <td key={`${item}`} className='position-relative'>
                <div className='d-flex align-items-center'>
                  <SortButton
                    columnKey={item}
                    sortParams={sortParams}
                    onChangeSort={onChangeSort}
                  />
                  <span>{getColName(item)}</span>
                </div>
              </td>
            ))}
        </tr>
      </thead>
    );
  }
}
