import React, { Component } from 'react';

import { DateParams } from '../../types';

interface DatePickerProps {
  dateParams: DateParams;
  onChangeDateRange: (date: { from: string; to: string }) => void;
}

export class DatePicker extends Component<DatePickerProps> {
  onChangeDate = (date: { from: string; to: string }): void => {
    const { from, to } = date;

    if (new Date(to) < new Date(from)) {
      this.props.onChangeDateRange({ from: to, to: from });
    } else {
      this.props.onChangeDateRange(date);
    }
  };

  render() {
    const { min, max, from, to } = this.props.dateParams;

    return (
      <div className='p-3 d-flex align-items-center'>
        <span>Select date: </span>
        <div className='d-flex flex-row pl-2 pr-2'>
          <input
            className='mr-2'
            type='date'
            min={min}
            max={max}
            value={from}
            onKeyPress={(event: React.SyntheticEvent) => event.preventDefault()}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              this.onChangeDate({ from: event.target.value, to })
            }
          />
          <input
            className='ml-2'
            type='date'
            min={min}
            max={max}
            value={to}
            onKeyPress={(event: React.SyntheticEvent) => event.preventDefault()}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              this.onChangeDate({ to: event.target.value, from })
            }
          />
        </div>
      </div>
    );
  }
}
