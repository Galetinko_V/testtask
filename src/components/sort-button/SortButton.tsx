import React, { useState, useEffect } from 'react';

import { ArrowUp, ArrowDown, ArrowExpand } from '../../icons';

import './style.css';

interface SortButtonProps {
  columnKey: string;
  sortParams: { key: string; direction: string };
  onChangeSort: (key: string, direction: string) => void;
}

enum DIRECTIONS {
  empty = '',
  up = 'up',
  down = 'down',
}

export const SortButton = ({
  onChangeSort,
  columnKey,
  sortParams,
}: SortButtonProps) => {
  const [direction, setDirection] = useState<string>(DIRECTIONS.empty);

  const onChangeDirection = (): void => {
    switch (direction) {
      case DIRECTIONS.empty:
      case DIRECTIONS.up:
        setDirection(DIRECTIONS.down);
        break;
      default:
        setDirection(DIRECTIONS.up);
    }
  };

  useEffect(() => {
    const isActive = sortParams.key === columnKey;

    if (isActive) {
      setDirection(sortParams.direction);
    }
  }, [columnKey, sortParams.key, sortParams.direction]);

  useEffect(() => {
    const isColumnSwitched = sortParams.key !== columnKey;

    if (isColumnSwitched) {
      setDirection('');
    }
  }, [sortParams, columnKey]);

  useEffect(() => {
    if (direction) {
      onChangeSort(columnKey, direction);
    }
  }, [direction, columnKey, onChangeSort]);

  return (
    <div
      className='h-100 d-flex flex-column align-item-center justify-content-center btn p-0'
      onClick={onChangeDirection}
    >
      {direction === DIRECTIONS.empty ? (
        <ArrowExpand />
      ) : direction === DIRECTIONS.up ? (
        <ArrowUp />
      ) : (
        <ArrowDown />
      )}
    </div>
  );
};
