import React from 'react';

import Table from '../table';
import DatePicker from '../date-picker';

import { ItemProps, DateParams } from '../../types';
import { DATA_URL } from '../../config';

import './style.css';

interface AppState {
  data: ItemProps[];
  loading: boolean;
  error: boolean;
  itemKeys: string[];
  dateParams: DateParams;
}

export class App extends React.Component<any, AppState> {
  state = {
    data: [],
    loading: false,
    error: false,
    itemKeys: [],
    dateParams: {
      from: '',
      to: '',
      min: '',
      max: '',
    },
  };

  componentDidMount() {
    this.setState({ loading: true });
    (async () => {
      try {
        const data = await this.fetchData(DATA_URL);
        if (data && data.length) {
          const dateParams = this.getDateRange(data);
          const itemKeys = Object.keys(data[0]);

          this.setState({ data, dateParams, itemKeys, loading: false });
        }
      } catch (err) {
        this.setState({ error: true, loading: false });
      }
    })();
  }

  fetchData = async (url: string) => {
    return await fetch(url).then((response) => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      return response.json();
    });
  };

  getDateRange = (data: ItemProps[]): DateParams => {
    const start = data[0].date;

    return data.reduce(
      (result, item) => {
        if (new Date(item.date) < new Date(result.min)) {
          result.min = item.date;
          result.from = item.date;
        }
        if (new Date(item.date) > new Date(result.max)) {
          result.max = item.date;
          result.to = item.date;
        }
        return result;
      },
      { min: start, max: start, from: start, to: start }
    );
  };

  onChangeDateRange = (range: { from?: string; to?: string }): void =>
    this.setState({ dateParams: { ...this.state.dateParams, ...range } });

  render() {
    const { data, loading, error, dateParams, itemKeys } = this.state;
    const _data = data.filter(
      (item: ItemProps) =>
        new Date(item.date) <= new Date(dateParams.to) &&
        new Date(item.date) >= new Date(dateParams.from)
    );

    return (
      <div className='App'>
        {loading && <div>Loading...</div>}
        {error && <div>Something went wrong</div>}
        {!(loading || error) && (
          <>
            <DatePicker
              onChangeDateRange={this.onChangeDateRange}
              dateParams={dateParams}
            />
            <Table data={_data} itemKeys={itemKeys} />
          </>
        )}
      </div>
    );
  }
}
